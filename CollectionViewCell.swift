//
//  CollectionViewCell.swift
//  VKFeed
//
//  Created by Илья Маркин on 17.05.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import UIKit
import SDWebImage

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var photos: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configurate(model: String){
        
        let photoUrl = URL(string: model)
        photos.sd_setImage(with: photoUrl)
    }
}
