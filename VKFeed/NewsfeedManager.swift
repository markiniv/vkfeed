//
//  NewsFeed.swift
//  VKFeed
//
//  Created by Илья Маркин on 06.05.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import Foundation


class Newsfeed {
    
    private static var dataSourse = NSArray()
    
    class func newsfeedGet(succesBlock: @escaping () -> Void, falureBlock: @escaping (Int) -> Void){
        
        let op = VKNewsfeedGetOperation(success: { (newsfeed) in
            
            dataSourse = newsfeed
            succesBlock()
        }, failure: falureBlock)
        
        OperationsManager.addOperation(op: op, cancellingQueue: true)
    }
    
    class func numberOfSections () -> Int {
        
        return dataSourse.count
    }
    
    
    class func numberOfModels (section : Int) -> Int {
        
        let data = dataSourse[section] as! [NewsItemContent]
        return data.count
    }
    
    
    class func model (atIndexPath indexPath : IndexPath) -> Any {
        
        let data = dataSourse[indexPath.section] as! [NewsItemContent]
        let output = data[indexPath.row]
        return output
    }
}
