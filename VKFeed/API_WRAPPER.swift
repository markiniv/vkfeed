//
//  API_Wrapper.swift
//  VKFeed
//
//  Created by Илья Маркин on 03.05.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import Foundation
import VK_ios_sdk

class API_WRAPPER{
    
    class func composeGenericHTTPRequest (argsDictinory: NSMutableDictionary, endpoint: String) -> URLRequest{
        
        var urlString = (Constants.API_Constants.kBase_URL as String) + (endpoint as String) + "?"
        let keysArray = argsDictinory.allKeys
        for key in keysArray{
            let value = argsDictinory[key]
            if key as! String == keysArray.last as! String{
                urlString += "\(key)=\(value!)"
                
            }else{
                urlString += "\(key)=\(value!)&"
            }
        }
        
        print("Наш запрос \(urlString)")
        let url = URL(string: urlString)!
        var requst = URLRequest(url: url)
        requst.httpMethod = "GET"
        return requst
    }
    
    
    class func genericCompetitionCallBack(data: Data?, response: URLResponse?, error: Error?, successBlock: (JSON) -> Void, falureBlock: (_ errorCode: Int) -> Void){
        
        if error != nil{
            falureBlock(-1001)
            return
        }
        if data != nil{
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                let result = JSON(json)
                print("Ответ сервера \(result)")
                successBlock(result)
            }catch{
                print(error)
                falureBlock(-1001)
            }
            
        }else{
            falureBlock(-1001)
        }
    }
}

extension API_WRAPPER {
    
    class func newsFeedGet(successBlock: @escaping (JSON) -> Void, falureBlock: @escaping (Int) -> Void) -> URLSessionDataTask{
        
        let argsDictinory = NSMutableDictionary()
        argsDictinory.setValue("post", forKey: Constants.Arguments.kAccessToken)
        argsDictinory.setValue(AuthManager.sharedInstance.getAccessToken(), forKey: Constants.Arguments.kAccessToken)
        argsDictinory.setValue(Constants.API_Constants.kVersion, forKey: Constants.Arguments.kVersion)
        
        let requst = API_WRAPPER.composeGenericHTTPRequest(argsDictinory: argsDictinory, endpoint: Constants.Endpoints.kNewsfeedGet)
        
        let task = URLSession.shared.dataTask(with: requst) { (data, response, error) in
            genericCompetitionCallBack(data: data, response: response, error: error, successBlock: successBlock, falureBlock: falureBlock)
        }
        
        task.resume()
        return task
    }
}

extension API_WRAPPER{
    
    class func getFriends( offset :  Int, count : Int, success: @escaping (JSON) -> Void, failure: @escaping (Int) -> Void){
        
        let argsDictionary = NSMutableDictionary()
        argsDictionary.setValue("\(offset)", forKey: Constants.Arguments.kOffset)
        argsDictionary.setValue("\(count)", forKey: Constants.Arguments.kCount)
        argsDictionary.setValue(AuthManager.sharedInstance.getAccessToken(), forKey: Constants.Arguments.kAccessToken)
        argsDictionary.setValue("photo_100", forKey: Constants.Arguments.kFileds)
        argsDictionary.setValue(Constants.API_Constants.kVersion, forKey: Constants.Arguments.kVersion)
        
        let requst = composeGenericHTTPRequest(argsDictinory: argsDictionary, endpoint: Constants.Endpoints.kFriendsGet)
        
        let task = URLSession.shared.dataTask(with: requst) { (data, response, error) in
            
            genericCompetitionCallBack(data: data, response: response, error: error, successBlock: success, falureBlock: failure)
        }
        task.resume()
    }
}

extension API_WRAPPER{
    
    class func messagesGetDialogs(count : Int, offset : Int, successBlock: @escaping (JSON) -> Void, falureBlock: @escaping (Int) -> Void) -> URLSessionDataTask{
        
        let argsDictinory = NSMutableDictionary()
        argsDictinory.setValue("\(count)", forKey: Constants.Arguments.kCount)
        argsDictinory.setValue("\(offset)", forKey: Constants.Arguments.kOffset)
        argsDictinory.setValue(AuthManager.sharedInstance.getAccessToken(), forKey: Constants.Arguments.kAccessToken)
        argsDictinory.setValue(Constants.API_Constants.kVersion, forKey: Constants.Arguments.kVersion)
        
        let requst = composeGenericHTTPRequest(argsDictinory: argsDictinory, endpoint: Constants.Endpoints.kMessagesGetDialogs)
        
        let task = URLSession.shared.dataTask(with: requst) { (data, response, error) in
            
            genericCompetitionCallBack(data: data, response: response, error: error, successBlock: successBlock, falureBlock: falureBlock)
        }
        task.resume()
        return task
    }

}

extension API_WRAPPER{
    
    class func usersGet(userID: String, success: @escaping (JSON) -> Void, failure: @escaping (Int) -> Void){
        
        let argsDictionary = NSMutableDictionary()
        argsDictionary.setValue(userID, forKey: Constants.Arguments.kIDs)
        argsDictionary.setValue("photo_100,online", forKey: Constants.Arguments.kFileds)
        let requst = composeGenericHTTPRequest(argsDictinory: argsDictionary, endpoint: Constants.Endpoints.kUserGet)
        
        let task = URLSession.shared.dataTask(with: requst) { (data, response, error) in
            
            genericCompetitionCallBack(data: data, response: response, error: error, successBlock: success, falureBlock: failure)
        }
        task.resume()
    }
}
