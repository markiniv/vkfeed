//
//  UserProfileItem.swift
//  VKFeed
//
//  Created by Илья Маркин on 30.04.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import Foundation

class UserProfileItem: NewsItemContent{
    
    let photoURL: String
    let userName: String
    
    init(photoURL: String, userName: String) {
        
        self.photoURL = photoURL
        self.userName = userName
    }
}
