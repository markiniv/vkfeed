//
//  BaseViewController.swift
//  SearchBar
//
//  Created by Илья Маркин on 20.05.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, View {

    @IBOutlet weak var activityIndicatorView : UIActivityIndicatorView!
    
    var presenter : Presenter?
    
    func handleErrorCode(code: Int){
        
        DispatchQueue.main.async {
            self.showAlert(code: code)
        }
    }
    
    private func showAlert(code: Int){
        
        var massage = ""
        
        if code == -1001 || code == -1009{
            
            massage = "Отсутсвует соединение с интернетом"
        }
        
        let alertController = UIAlertController(title: nil, message: massage, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "ok", style: .default, handler: nil)
        alertController.addAction(alertAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func assignPresenter ( presenter : Presenter ) -> Void{
        
        self.presenter = presenter
        presenter.viewLoaded(view: self)
    }
    func reloadData() -> Void{
        
    }

    
    func addLoader () -> Void{
        
        showIndicator()
    }
    func removeLoader () -> Void{
        
        hideIndicator()
    }
    func addNegativeScreen () -> Void{
        
    }
    func removeNegativeScreen () -> Void{
       
    }
    
    private func showIndicator(){
        
        DispatchQueue.main.async {
            
            self.activityIndicatorView.hidesWhenStopped = true
            self.activityIndicatorView.startAnimating()
        }
    }
    
    private func hideIndicator(){
        
        DispatchQueue.main.async {
            
            self.activityIndicatorView.stopAnimating()
        }
    }
}
