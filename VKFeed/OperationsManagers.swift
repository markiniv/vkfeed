//
//  OperationsManagers.swift
//  newVk
//
//  Created by Илья Маркин on 03.06.17.
//  Copyright © 2017 Sergey Kochetov. All rights reserved.
//

import Foundation

class OperationsManager{
    
    private static let buisnessLogicOperationsQueue = OperationQueue()
    
    class func addOperation (op: Operation, cancellingQueue: Bool){
        
        buisnessLogicOperationsQueue.maxConcurrentOperationCount = 1
        
        if cancellingQueue{
            
            buisnessLogicOperationsQueue.cancelAllOperations()
        }
        
        buisnessLogicOperationsQueue.addOperation(op)
    }
}
