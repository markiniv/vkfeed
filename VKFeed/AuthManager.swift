//
//  AuthManager.swift
//  VKFeed
//
//  Created by Илья Маркин on 23.04.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import Foundation
import VK_ios_sdk

class AuthManager : NSObject {
    
    //Singleton
    static let sharedInstance = AuthManager()
    
    var successBlock : (() -> Void)?
    var failureBlock : (() -> Void)?
    
    weak var controller : UIViewController?
    
    var sdkInstance : VKSdk!
}

//MARK : - CheckURL
extension AuthManager {
    
    func proccess(passedUrl : URL, application: String) -> Bool {
        
        return VKSdk.processOpen(passedUrl, fromApplication: application)
    }
}

extension AuthManager
{
    func login ( withController controller : UIViewController , success : @escaping () -> Void , failure : @escaping () -> Void )
    {
        if ( getAccessToken() != "" )
        {
            success()
            return
        }
        
        self.controller = controller
        self.successBlock = success
        self.failureBlock = failure
        
        sdkInstance = VKSdk.initialize(withAppId: Constants.AppConstants.kAPP_ID)
        sdkInstance.register(self)
        sdkInstance.uiDelegate = self
        
        VKSdk.authorize(["friends" , "photos" , "audio" , "video" , "messages" , "wall" , "offline"])
    }
}

extension AuthManager: VKSdkDelegate, VKSdkUIDelegate {
    
    func vkSdkUserAuthorizationFailed() {
        
        failureBlock!()
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        
    }
    
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        
        self.controller?.present(controller, animated: true, completion: nil)
    }
    
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        
        if let accessToken = result.token
        {
            setAccessToken ( token: accessToken.accessToken )
            self.successBlock?()
        }
        else
        {
            self.failureBlock?()
        }
    }
    
    private func setAccessToken ( token : String )
    {
        UserDefaults.standard.set(token, forKey: "X-Access-Token")
        UserDefaults.standard.synchronize()
    }
}

extension AuthManager {
    
	   func getAccessToken() -> String {
        
        if let token = UserDefaults.standard.object(forKey: "X-Access-Token")
        {
            return token as! String
        }
        
        return ""
    }
}
