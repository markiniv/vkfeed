//
//  VKFMessagesDialogsViewController.swift
//  VKFeed
//
//  Created by Илья Маркин on 26.05.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import UIKit

class VKFMessagesDialogsViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    
//    var model = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if self.presenter == nil{
            
            DependencyInjector.assignPresenter(view: self)
        }
        
        let myDict : NSDictionary = ["substring" : ""]
        presenter?.provideData(data: myDict)
        super.viewWillAppear(animated)
    }
    
    override func reloadData() {
        
        DispatchQueue.main.async {
            
            self.tableView.reloadData()
        }
    }
}

extension VKFMessagesDialogsViewController: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (presenter?.numberOfModels(inSection: section))!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = presenter?.getModel(atIndexPath: indexPath)  as! VKFMessageModel
        let cell = tableView.dequeueReusableCell(withIdentifier: "messageCell", for: indexPath) as!VKFMessagesGetDialogsTableViewCell
        cell.configureSelf(withDataModel: model, count: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let height : CGFloat = 76
        //        tableView.estimatedRowHeight = 10
        return height
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        
//        if (presenter?.willGetModel(atIndexPath: indexPath))!{
//            
//            self.viewWillAppear(false)
//        }
//    }
}

extension VKFMessagesDialogsViewController{
    
    func configureView(){
        
        let MessageNib = UINib(nibName: "VKFMessagesGetDialogsTableViewCell", bundle: Bundle.main)
        self.tableView.register(MessageNib, forCellReuseIdentifier: "messageCell")
    }
}
