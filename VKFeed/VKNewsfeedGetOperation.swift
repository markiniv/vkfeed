//
//  VKNewsfeedGetOperation.swift
//  VKFeed
//
//  Created by Илья Маркин on 07.06.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import Foundation

class VKNewsfeedGetOperation : Operation{
    
    var success : (NSArray) -> Void
    var failure : (Int) -> Void
    
    var internetTask : URLSessionDataTask?
    
    init(success : @escaping (NSArray) -> Void, failure : @escaping (Int) -> Void) {
        
        self.success = success
        self.failure = failure
    }
    
    override func cancel() {
        
        internetTask?.cancel()
        print("задача интернета отменена")
    }
    
    override func main() {
        
        let semaphore = DispatchSemaphore(value: 0)
        internetTask = API_WRAPPER.newsFeedGet(successBlock: { (response) in
            
            let responsesArray = NSMutableArray()
            
            
            
            let items = response["response"]["items"].arrayValue
            let groups = response["response"]["groups"].arrayValue
            let profiles = response["response"]["profiles"].arrayValue
            var postID = ""
            
            for item in items{
                
                
                if item["type"].stringValue == "post" && postID != item["post_id"].stringValue{
                    
                    var news = [NewsItemContent]()
                    let sourceID = item["source_id"].intValue
                    let photos = item["attachments"].arrayValue
                    let copyHistory = item["copy_history"].arrayValue
                    postID = item["post_id"].stringValue
                    
                    if 0 > sourceID{
                        for group in groups{
                            
                            if -sourceID == group["id"].intValue{
                                
                                let profile = UserProfileItem(photoURL: group["photo_100"].stringValue, userName: group["name"].stringValue)
                                news.append(profile)
                            }
                        }
                    }else{
                        
                        for profile in profiles{
                            
                            if sourceID == profile["id"].intValue{
                                
                                let userName = profile["first_name"].stringValue + " " + profile["last_name"].stringValue
                                
                                let profile = UserProfileItem(photoURL: profile["photo_100"].stringValue, userName: userName)
                                news.append(profile)
                            }
                        }
                    }
                    
                    let text = TextItem(text: item["text"].stringValue)
                    
                    if copyHistory.count != 0{
                        
                        let userID = copyHistory[0]["from_id"].intValue
                        
                        if 0 > userID{
                            for group in groups{
                                
                                if -userID == group["id"].intValue{
                                    
                                    let profile = UserProfileItem(photoURL: group["photo_100"].stringValue, userName: group["name"].stringValue)
                                    news.append(profile)
                                }
                            }
                        }else{
                            
                            for profile in profiles{
                                
                                if sourceID == profile["id"].intValue{
                                    
                                    let userName = profile["first_name"].stringValue + " " + profile["last_name"].stringValue
                                    
                                    let profile = UserProfileItem(photoURL: profile["photo_100"].stringValue, userName: userName)
                                    news.append(profile)
                                }
                            }
                        }
                        let text = TextItem(text: copyHistory[0]["text"].stringValue)
                        
                        news.append(text)                        
                    }
                    var photosArray = [String]()
                    
                    for photo in photos{
                        
                        if photo["type"].stringValue == "photo"{
                        
                            if photo["photo"]["photo_807"].stringValue != ""{
                                
                            photosArray.append(photo["photo"]["photo_807"].stringValue)
                            }else{
                                 photosArray.append(photo["photo"]["photo_604"].stringValue)
                            }
                        }
                    }
                    
                    let photoItem = PhotoItem(photos: photosArray)
                    let likes = LikesItems(likesCount: item["likes"]["count"].stringValue, repostsCount: item["reposts"]["count"].stringValue)
                    
                    
                    news.append(text)
                    news.append(photoItem)
                    news.append(likes)
                    
                    responsesArray.add(news)
                }
            }
            
            if self.isCancelled != true{
                
                self.success(responsesArray)
            }else{
                
                self.success(NSArray())
            }
            
            _ = semaphore.signal()
        }, falureBlock: { (code) in
            
            self.failure(code)
            _ = semaphore.signal()
        })
        
         _ = semaphore.wait(timeout: .distantFuture)
    }
}
