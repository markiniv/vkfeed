//
//  LikesItem.swift
//  VKFeed
//
//  Created by Илья Маркин on 30.04.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import Foundation

class LikesItems: NewsItemContent{
    
    let likesCount: String
    let repostsCount: String
    
    init(likesCount: String, repostsCount: String) {
        
        self.likesCount = likesCount
        self.repostsCount = repostsCount
    }
    
}
