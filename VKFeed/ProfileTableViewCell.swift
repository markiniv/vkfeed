//
//  ProfileTableViewCell.swift
//  VKFeed
//
//  Created by Илья Маркин on 30.04.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var iamge: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    func configurate (model: UserProfileItem){
        
        let photoUrl = URL(string: model.photoURL)
        iamge.sd_setImage(with: photoUrl)
        label.text = model.userName
    }
}
