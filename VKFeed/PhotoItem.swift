//
//  PhotoItem.swift
//  VKFeed
//
//  Created by Илья Маркин on 30.04.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import Foundation

class PhotoItem: NewsItemContent{
    
    var photos: [String]
    var Vertical : Bool?
    
    init(photos: [String]) {
        self.photos = photos
    }
}
