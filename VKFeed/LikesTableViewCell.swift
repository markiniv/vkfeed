//
//  LikesTableViewCell.swift
//  VKFeed
//
//  Created by Илья Маркин on 03.05.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import UIKit

class LikesTableViewCell: UITableViewCell {

    @IBOutlet weak var likes: UILabel!
    @IBOutlet weak var reposts: UILabel!
    
    func configurate(model: LikesItems){
        
        likes.text = model.likesCount
        reposts.text = model.repostsCount
    }
}
