//
//  FeedTableViewController.swift
//  VKFeed
//
//  Created by Илья Маркин on 30.04.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import UIKit

class FeedTableViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var count = 0
    
    override func viewDidLoad() {
        
        tableView.delegate = self
        tableView.dataSource = self
        
        configureView()
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        AuthManager.sharedInstance.login(withController: self, success: {
            print(AuthManager.sharedInstance.getAccessToken())
            print("success")
        }) {
            print("error")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if self.presenter == nil{
            
            DependencyInjector.assignPresenter(view: self)
        }
        
        let myDict : NSDictionary = ["substring" : ""]
        presenter?.provideData(data: myDict)
        super.viewWillAppear(animated)
    }
    
    override func reloadData() {
        
        DispatchQueue.main.async {
            
            self.tableView.reloadData()
        }
    }
}

extension FeedTableViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {

        return (presenter?.numberOfSection())!
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (presenter?.numberOfModels(inSection: section))!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = presenter?.getModel(atIndexPath: indexPath)
        
        if  let profile = data as? UserProfileItem{
            
            count = 0
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "profileCell", for: indexPath) as! ProfileTableViewCell
            
            cell.configurate(model: profile)
            return cell
            
        }else if let profile = data as? TextItem{
            
            count = 0
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCell", for: indexPath) as! TextTableViewCell
            
            
            cell.configurate(model: profile)
            return cell
            
        }else if let profile = data as? PhotoItem{
            
            count = profile.photos.count
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "photosCell", for: indexPath) as! TableViewCell
            cell.configurate(with: profile, size: Int(tableView.frame.width))
            return cell
            
        }
        else if let profile = data as? LikesItems{
            
            count = 0
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "likesCell", for: indexPath) as! LikesTableViewCell
            
            cell.configurate(model: profile)
            return cell
            
        } else{
            
            count = 0
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "likesCell", for: indexPath) as! LikesTableViewCell
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch count {
        case 1,4,7,9:
            tableView.estimatedRowHeight = 10
            let height = tableView.frame.width
            return height
        case 2,3:
            tableView.estimatedRowHeight = 10
            let height = tableView.frame.width / 2
            return height
        case 5,6:
            tableView.estimatedRowHeight = 10
            let height = tableView.frame.width * 1.5
            return height
        case 8,10:
            tableView.estimatedRowHeight = 10
            let height = tableView.frame.width * 2
            return height
        default:
            tableView.estimatedRowHeight = 10
            return UITableViewAutomaticDimension
        }
    }
}

extension FeedTableViewController{
    
    func configureView(){
        
        self.tableView.separatorStyle = .none
        let textCellNib = UINib(nibName: "TextTableViewCell", bundle: Bundle.main)
        self.tableView.register(textCellNib, forCellReuseIdentifier: "textCell")
        
        let profileCellNib = UINib(nibName: "ProfileTableViewCell", bundle: Bundle.main)
        self.tableView.register(profileCellNib, forCellReuseIdentifier: "profileCell")
        
        let likesCellNib = UINib(nibName: "LikesTableViewCell", bundle: Bundle.main)
        self.tableView.register(likesCellNib, forCellReuseIdentifier: "likesCell")
        
        let photosCellNib = UINib(nibName: "TableViewCell", bundle: Bundle.main)
        self.tableView.register(photosCellNib, forCellReuseIdentifier: "photosCell")
        
    }
}
