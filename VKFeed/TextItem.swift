//
//  TextItem.swift
//  VKFeed
//
//  Created by Илья Маркин on 30.04.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import Foundation

class TextItem: NewsItemContent{
    
    var text: String 
    
    init(text: String) {
        
        self.text = text
    }
}
