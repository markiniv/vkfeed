//
//  BL_StatusBarActivityIndicator.swift
//  DateUp
//
//  Created by Nikita Pronchik on 22.06.15.
//  Copyright (c) 2015 Beet Lab. All rights reserved.
//

import UIKit

class BL_StatusBarActivityIndicator: NSObject
{
    var indicatorCounter : Int = 0
    
    static let singletonInstance = BL_StatusBarActivityIndicator ()
    
    func fireUpIndicator () -> Void
    {
        objc_sync_enter(self)
        self.indicatorCounter += 1
        objc_sync_exit(self)
        updateIndicator()
    }
    
    func switchOffIndicator () -> Void
    {
        objc_sync_enter ( self )
        if ( self.indicatorCounter > 0 )
        {
            self.indicatorCounter -= 1
        }
        objc_sync_exit(self)
        updateIndicator()
    }
    
    func removeIndicator () -> Void
    {
        DispatchQueue.main.async
            {
                self.indicatorCounter = 0
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    private func updateIndicator() -> Void
    {
        DispatchQueue.main.async
            {
                print("indicator counter \(self.indicatorCounter)")
                UIApplication.shared.isNetworkActivityIndicatorVisible = self.indicatorCounter > 0
        }
    }
}
