//
//  VKFDilagosGet.swift
//  VKFeed
//
//  Created by Илья Маркин on 05.06.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import Foundation

class VKFDialogsGetOperation : Operation{
    
    var count : Int
    var offset : Int
    var success : (NSArray) -> Void
    var failure : (Int) -> Void
    
    var internetTask : URLSessionDataTask?
    
    init(count : Int, offset : Int, success : @escaping (NSArray) -> Void, failure : @escaping (Int) -> Void) {
        
        self.count = count
        self.offset = offset
        self.success = success
        self.failure = failure
    }
    
    override func cancel() {
        
        internetTask?.cancel()
        print("задача интернета отменена")
    }
    
    override func main() {
        
        let semaphore = DispatchSemaphore(value: 0)
        
        internetTask = API_WRAPPER.messagesGetDialogs(count: count, offset: offset, successBlock: { (response) in
            
            let responsesArray = NSMutableArray()
            var messageArray = [VKFMessageModel]()
            
            let messages = response["response"]["items"].arrayValue
             var usersID = ""
            
            for item in messages{
                
                let message = item["message"]
                let chatActive =  message["chat_active"].arrayValue
                let repost = message["fwd_messages"].arrayValue
                var text = ""
                if message["body"].stringValue != ""{
                    
                    text = message["body"].stringValue
                }else if repost.count != 0{
                    
                    text = "Пересланные сообщения"
                }else if message["attachments"].arrayValue.count != 0{
                    
                    text = message["attachments"][0]["type"].stringValue
                }
                
                var name : String = ""
                var avatar = [String]()
                var id : String = ""
                var active = [String]()
                
                
                
                if chatActive.isEmpty{
                    
                    id = message["user_id"].stringValue
                    
                    if usersID != ""{
                        
                        usersID += ",\(id)"
                    }else{
                        
                        usersID += id
                    }
                }else{
                    name = message["title"].stringValue
                    avatar.append(message["photo_100"].stringValue)
                    
                    if chatActive.count < 5 && message["photo_100"].stringValue == ""{
                        for i in 0..<chatActive.count{
                            
                            active.append(chatActive[i].stringValue)
                            
                            if usersID != ""{
                                
                                usersID += ",\(chatActive[i].stringValue)"
                            }else{
                                
                                usersID += chatActive[i].stringValue
                            }
                        }
                    }else if message["photo_100"].stringValue == ""{
                        
                        for i in 0..<4 {
                            
                            active.append(chatActive[i].stringValue)
                            
                            if usersID != ""{
                                
                                usersID += ",\(chatActive[i].stringValue)"
                            }else{
                                
                                usersID += chatActive[i].stringValue
                            }
                        }
                        
                    }
                }
                let dialog = VKFMessageModel(message: text, name: name, avatarImage: avatar, id: id, chatActive: active)
                messageArray.append(dialog)
            }
            
            print("print количества диалогов \(messageArray.count)")
            
            API_WRAPPER.usersGet(userID: usersID, success: { (response) in
                
                for user in messageArray{
                    
                    let peoples = response["response"].arrayValue
                    
                    if user.chatActive.isEmpty{
                        
                        for people in peoples{
                            
                            if user.id == people["uid"].stringValue{
                                
                                let name = people["first_name"].stringValue + " " + people["last_name"].stringValue
                                var avatarImage  = [String]()
                                avatarImage.append(people["photo_100"].stringValue)
                                
                                let dialog = VKFMessageModel(message: user.message, name: name, avatarImage: avatarImage, id: user.id, chatActive: user.chatActive)
                                responsesArray.add(dialog)
                                
                            }
                        }
                    }else {
                        
                        var avatarImage = [String]()
                        
                        if user.avatarImage[0] == ""{
                            
                            print("print количества юезеров \(user.chatActive.count)")
                            
                            
                            for j in 0..<user.chatActive.count{
                                for people in peoples{
                                    
                                    if user.chatActive[j] == people["uid"].stringValue{
                                        
                                        avatarImage.append(people["photo_100"].stringValue)
                                    }
                                }
                            }
                            
                            let dialog = VKFMessageModel(message: user.message, name: user.name, avatarImage: avatarImage, id: user.id, chatActive: user.chatActive)
                            print("print количества фоток после парсинга \(dialog.avatarImage.count)")
                            responsesArray.add(dialog)
                        }else{
                            responsesArray.add(user)
                        }
                    }
                }
                if self.isCancelled != true{
                    
                    self.success(responsesArray)
                }else{
                    
                    self.success(NSArray())
                }
                _ = semaphore.signal()
            }, failure: { (code) in
                
                
            })
            
            
        }, falureBlock: { (code) in
            
            self.failure(code)
            _ = semaphore.signal()
        })
        
        _ = semaphore.wait(timeout: .distantFuture)
    }
}
