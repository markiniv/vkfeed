//
//  View.swift
//  newVk
//
//  Created by MacAdmin on 03.06.17.
//  Copyright © 2017 Sergey Kochetov. All rights reserved.
//

import Foundation

protocol View : class
{
    func assignPresenter ( presenter : Presenter ) -> Void
    func reloadData() -> Void
    func handleErrorCode ( code : Int ) -> Void
    func addLoader () -> Void
    func removeLoader () -> Void
    func addNegativeScreen () -> Void
    func removeNegativeScreen () -> Void
}
