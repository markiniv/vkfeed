//
//  VKFPhotosCollectionViewCell.swift
//  VKFeed
//
//  Created by Илья Маркин on 02.06.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import UIKit

class VKFPhotosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var avatarImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configurate(model: String){
        
        let photoUrl = URL(string: model)
        avatarImage.sd_setImage(with: photoUrl)
    }
}
