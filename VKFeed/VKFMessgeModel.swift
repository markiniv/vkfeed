//
//  VKFMessgeModel.swift
//  VKFeed
//
//  Created by Илья Маркин on 26.05.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import Foundation

class VKFMessageModel{
    
    var message : String
    var name : String
    var avatarImage : [String]
    var id : String
    var chatActive : [String]
    
    init(message: String, name: String, avatarImage: [String], id : String, chatActive: [String]) {
        
        self.message = message
        self.name = name
        self.avatarImage = avatarImage
        self.id = id
        self.chatActive = chatActive
    }
}
