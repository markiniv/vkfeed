//
//  NewsfeedPresenter.swift
//  VKFeed
//
//  Created by Илья Маркин on 13.06.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import Foundation

class NewsfeedPresenter : Presenter{
    
    
    func willGetModel(atIndexPath: IndexPath) -> Bool {
        
        return true
    }

    
    weak var view : View?
    
    func viewLoaded ( view : View ) -> Void{
        
        self.view = view
    }
    func provideData ( data : NSDictionary ) -> Void{
        
        getData()
    }
    
    func numberOfSection () -> Int{
        
        return Newsfeed.numberOfSections()
    }
    func numberOfModels ( inSection : Int ) -> Int{
        
        return Newsfeed.numberOfModels(section: inSection)
    }
    func getModel ( atIndexPath : IndexPath ) -> AnyObject{
        
        return Newsfeed.model(atIndexPath: atIndexPath) as AnyObject
    }
    
    private func getData(){
        
        view?.addLoader()
        Newsfeed.newsfeedGet(succesBlock: { 
            
            self.view?.removeLoader()
            self.view?.reloadData()
        }) { (code) in
            
            self.view?.removeLoader()
            self.view?.handleErrorCode(code: code)
        }
    }
}
