//
//  VKFMessagesGetDialogsTableViewCell.swift
//  VKFeed
//
//  Created by Илья Маркин on 26.05.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import UIKit
import SDWebImage

class VKFMessagesGetDialogsTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    var count : IndexPath?
    var photoArray = [String]()
    
    override func awakeFromNib() {
            super.awakeFromNib()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        let photos = UINib(nibName: "VKFPhotosCollectionViewCell", bundle: Bundle.main)
        collectionView.register(photos, forCellWithReuseIdentifier: "VKFPhotosIdentifier")
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }

    }
    
    func configureSelf( withDataModel model: VKFMessageModel, count : IndexPath) {
        
        messageLabel.text = model.message
        nameLabel.text = model.name
        photoArray = model.avatarImage
        self.count = count
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
}

extension VKFMessagesGetDialogsTableViewCell: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
      return VKFMassegesManager.avatarCount(indexPath: count!)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VKFPhotosIdentifier", for: indexPath) as! VKFPhotosCollectionViewCell
        let model = photoArray[indexPath.row]
        cell.configurate(model: model)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
 
        switch photoArray.count {
        case 1:
            let widht = collectionView.frame.width - 0.5
            let hight = widht
            return CGSize(width: widht, height: hight)
        case 2:
            let widht = collectionView.frame.width / 2 - 0.5
            let hight = collectionView.frame.height
            return CGSize(width: widht, height: hight)
            
        case 4:
            let widht = collectionView.frame.width / 2 - 0.5
            let hight = widht
            return CGSize(width: widht, height: hight)
        default:
            let widht = collectionView.frame.width / 2 - 0.5
            let hight = widht
            return CGSize(width: widht, height: hight)
        }
    
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.25
    }
}
