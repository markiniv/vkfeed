//
//  Presenter.swift
//  newVk
//
//  Created by MacAdmin on 03.06.17.
//  Copyright © 2017 Sergey Kochetov. All rights reserved.
//

import Foundation

protocol Presenter : class
{
    func viewLoaded ( view : View ) -> Void
    func provideData ( data : NSDictionary ) -> Void
    func numberOfModels ( inSection : Int ) -> Int
    func numberOfSection () -> Int
    func getModel ( atIndexPath : IndexPath ) -> AnyObject
    func willGetModel( atIndexPath : IndexPath ) -> Bool
}
