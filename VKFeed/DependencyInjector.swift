//
//  DependencyInjector.swift
//  newVk
//
//  Created by MacAdmin on 03.06.17.
//  Copyright © 2017 Sergey Kochetov. All rights reserved.
//

import Foundation

class DependencyInjector
{
    class func assignPresenter ( view : View )
    {
        var presenter : Presenter?
        
        if view is FeedTableViewController
        {
            presenter = NewsfeedPresenter()
        }
        
        if view is VKFMessagesDialogsViewController{
            
            presenter = VKDialogPresenter()
        }
        
        if presenter != nil
        {
            view.assignPresenter(presenter: presenter!)
        }
    }
}
