//
//  Constants.swift
//  VKFeed
//
//  Created by Илья Маркин on 23.04.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import Foundation


class Constants {
    
    class AppConstants{
        
        static let kAPP_ID = "6015736"
        static let kBaseURL = "https://api.vk.com/method/"
    }
    
    class API_Constants{
        
        static let kBase_URL = "https://api.vk.com/method/"
        static let kVersion = "5.64"
    }
    
    class Endpoints{
        
        static let kNewsfeedGet = "newsfeed.get"
        static let kFriendsGet = "friends.get"
        static let kMessagesGetDialogs = "messages.getDialogs"
        static let kUserGet = "users.get"
    }
    
    class Arguments{
        
        static let kFilters = "filters"
        static let kVersion = "v"
        static let kAccessToken = "access_token"
        static let kStartFrom = "start_from"
        static let kFileds = "fields"
        static let kCount = "count"
        static let kOffset = "offset"
        static let kIDs = "user_ids"
    }
}
