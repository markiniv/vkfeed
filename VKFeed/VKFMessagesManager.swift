//
//  VKFFriendsManager.swift
//  VKFeed
//
//  Created by Илья Маркин on 27.05.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import Foundation


class VKFMassegesManager{
    
    private static var modelArray = NSArray()
    
    class func getDialogs(count: Int, offset : Int, success: @escaping ()->Void, failure: @escaping (Int)->Void){
        
        let op = VKFDialogsGetOperation(count: count, offset: offset, success: { (data) in
            
            self.modelArray = data
            print("count model array \(modelArray.count)")
            
            success()
        }, failure: ({print($0)}))
        
          OperationsManager.addOperation(op: op, cancellingQueue: true)
        
    }
    
    class func numberOfModels() -> Int{
        
        return modelArray.count
    }
    
    class func model(indexPath: IndexPath) -> VKFMessageModel{
        
        return modelArray[indexPath.row] as! VKFMessageModel
    }
    
    class func avatarCount(indexPath: IndexPath) -> Int{
        
        let model = modelArray[indexPath.row] as! VKFMessageModel
        return model.avatarImage.count
    }
    
    class func avatarModel(indexPath: IndexPath) -> [String]{
        
        let model = modelArray[indexPath.row] as! VKFMessageModel
        
           print("print indexPath.row in manager \(indexPath.row)")
        return model.avatarImage
    }
    
    class func wilGetModel( atIndexPath : IndexPath ) -> Bool{
        
        if atIndexPath.row == modelArray.count - 15{
            
            return true
        }else{
            
            return false
        }
    }
}
