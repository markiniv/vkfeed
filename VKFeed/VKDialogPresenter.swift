//
//  VKDialogPresenter.swift
//  VKFeed
//
//  Created by Илья Маркин on 14.06.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import Foundation

class VKDialogPresenter : Presenter{
    
    var offset = 0
    
    weak var view : View?
    
    func viewLoaded ( view : View ) -> Void{
        
        self.view = view
    }
    func provideData ( data : NSDictionary ) -> Void{
        
        
        getData(offset: offset)
        offset += 20
    }
    func numberOfModels ( inSection : Int ) -> Int{
        
        return VKFMassegesManager.numberOfModels()
    }
    func numberOfSection () -> Int{
        
        return 1
    }
    func getModel ( atIndexPath : IndexPath ) -> AnyObject{
        
        return VKFMassegesManager.model(indexPath: atIndexPath)
    }
    
    func willGetModel( atIndexPath : IndexPath ) -> Bool{
        
        return VKFMassegesManager.wilGetModel(atIndexPath: atIndexPath)
    }
    
    private func getData(offset: Int){
        
        view?.addLoader()
        VKFMassegesManager.getDialogs(count: 20 + offset, offset: offset, success: {
            
            self.view?.removeLoader()
            self.view?.reloadData()
        }) { (code) in
            
            self.view?.removeLoader()
            self.view?.handleErrorCode(code: code)
        }
    }
}
