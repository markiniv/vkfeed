//
//  PhotosCollection.swift
//  VKFeed
//
//  Created by Илья Маркин on 18.05.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import UIKit

class PhotosCollection: UICollectionView {

    override func awakeFromNib() {
        
        let phtos = UINib(nibName: "CollectionViewCell", bundle: Bundle.main)
        register(phtos, forCellWithReuseIdentifier: "photosCollection")
    }
    
    override func cellForItem(at indexPath: IndexPath) -> UICollectionViewCell? {
        
        let cell = dequeueReusableCell(withReuseIdentifier: "photosCollection", for: indexPath) as! CollectionViewCell
        
        
        return cell
    }

}
