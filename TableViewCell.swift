//
//  TableViewCell.swift
//  VKFeed
//
//  Created by Илья Маркин on 17.05.17.
//  Copyright © 2017 Илья Маркин. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    var photoArray = [String]()
    var size : Int = 0
    
    @IBOutlet weak var collectionView: UICollectionView!

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = self
        collectionView.delegate = self
        
        let phtos = UINib(nibName: "CollectionViewCell", bundle: Bundle.main)
        collectionView.register(phtos, forCellWithReuseIdentifier: "photosCollection")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configurate(with model: PhotoItem, size: Int){
        
        self.photoArray = model.photos
        self.size = size
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
}

extension TableViewCell: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return photoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photosCollection", for: indexPath) as! CollectionViewCell
        let model = photoArray[indexPath.row]
        cell.configurate(model: model)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if photoArray.count == 1{
            let widht = collectionView.frame.width  - 2
            let hight = widht
            return CGSize(width: widht, height: hight)
        } else if photoArray.count % 2 == 0{
            let widht = collectionView.frame.width / 2 - 2
            let hight = widht
            return CGSize(width: widht, height: hight)
        }else{
            
            if indexPath.row == 0{
                
                let widht = collectionView.frame.width
                let hight = widht / 2
                return CGSize(width: widht, height: hight)
            }else{
                let widht = collectionView.frame.width / 3 - 2
                let hight = widht
                return CGSize(width: widht, height: hight)
            }
        }
       
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }

}
